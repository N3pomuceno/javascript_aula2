const precos = [
    "Crédito",
    "R$ 200",
    "R$ 400",
    "Contas a pagar",
    "R$ 300",
    "R$ 400",
    "Meus Dados"
];
const precosFiltro = precos.filter(preco => preco.includes("R$"));
console.log(precosFiltro);

// outro exemplo

const numbers = [1,6,7];
console.log(numbers.map(num => num*2));

// outro exemplo

const filtro = numbers.filter(num =>{
    if (num%2 === 0){
        return num
    }
})
console.log(filtro);

// Mesmo exemplo outra forma
const filtr2 = numbers.filter(num =>num% 2 ===0)
console.log(filtr2);

// outro exemplo

const  pokemons = ["Gengar", "Alakazam", "Machamp", "Snorlax"];
function gottaCath(elem){
    console.log("Gotta cath " + elem);
}
pokemons.forEach(gottaCath);
console.log("Gotta cath'em all!");