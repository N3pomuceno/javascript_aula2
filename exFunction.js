// função padrão
function multiplicar(a,b){
    return a*b;
}

// função anônima
let minhaFuncaoAnonima = function(){
    console.log("Bom dia familia");
}
minhaFuncaoAnonima();

// função arrow
const soma = (a,b) => {
    return a+b;
}  
console.log(soma(4,4));

// ou pode ser escrito tbm da forma:
// const soma = (a,b) => a+b;
// console.log(soma(4,4));

// ou quando tem somente um parâmetro:
// const soma = (a) => a;
// console.log(soma(4));

// função callback
function minhaCallback(num){
    return num%7;
}
function comACallback(outroNumero, minhaFuncaoCallback){
    let callback = minhaFuncaoCallback(outroNumero);
    return callback;
}
console.log(comACallback (8,minhaCallback))

// callback com arrow - não terminaram o exemplo, elese disseram para tentar fazer fun~]ap anônima com arrow function
// function minhaCallback(num){
//     return num%7;
// }


// function comACallback(outroNumero, num => num%7) {
//     let callback = minhaFuncaoCallback(outroNumero);
//     return callback;
// }
// console.log( (num) => )